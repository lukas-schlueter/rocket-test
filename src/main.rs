#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate log;
#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_contrib;

use rocket::fairing::AdHoc;
use rocket_contrib::json::Json;
use rocket_contrib::serve::StaticFiles;
use rocket_contrib::templates::Template;

use telegram_bot_raw::*;

use data::DbConn;

mod schema;

embed_migrations!();

#[allow(clippy::identity_conversion)]
mod data {
    #[database("sqlite")]
    pub struct DbConn(diesel::SqliteConnection);
}

#[get("/")]
const fn index() -> &'static str {
    "Hello, world!"
}

#[post("/tgWebhook", format = "json", data = "<update>")]
fn tg_webhook(update: Json<telegram_bot_raw::Update>) {
    let inner = update.into_inner();
    if let UpdateKind::Message(message) = inner.clone().kind {
        let send_message = message.text_reply(format!("{:#?}", inner));
        send(send_message);
    }
}

fn send(req: impl telegram_bot_raw::Request + std::fmt::Debug) {
    let req = req.serialize().unwrap();
    let uri = &req.url.url(&TG_BOT_TOKEN);
    if let telegram_bot_raw::Body::Json(body) = req.body {
        let request = CLIENT
            .post(uri)
            .header(
                reqwest::header::CONTENT_TYPE,
                reqwest::header::HeaderValue::from_static("application/json"),
            )
            .body(body);
        request.send().unwrap();
    };
}

lazy_static! {
    static ref CLIENT: reqwest::Client = reqwest::Client::new();
    static ref TG_BOT_TOKEN: String = std::env::var("TELEGRAM_BOT_TOKEN").unwrap();
}

fn main() {
    dotenv::dotenv().ok();

    rocket::ignite()
        .attach(DbConn::fairing())
        .attach(AdHoc::on_attach("Database Migrations", |rocket| {
            let conn = DbConn::get_one(&rocket).expect("database donnection");
            match embedded_migrations::run(&*conn) {
                Ok(()) => Ok(rocket),
                Err(e) => {
                    error!("Failed to run database migrations: {:?}", e);
                    Err(rocket)
                }
            }
        }))
        .mount("/", routes![index, tg_webhook])
        .mount("/static", StaticFiles::from("static"))
        .attach(Template::fairing())
        .launch();
}
