#![allow(clippy::identity_conversion)]

table! {
    tasks (id) {
        id -> Integer,
        description -> Text,
        completed -> Bool,
    }
}
