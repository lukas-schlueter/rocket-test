-- Your SQL goes here

CREATE TABLE tasks
(
  id          INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  description VARCHAR NOT NULL,
  completed   BOOLEAN NOT NULL DEFAULT 0
);