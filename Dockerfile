FROM rustlang/rust:nightly as builder

RUN apt-get install libsqlite3-dev

ARG NAME=app
RUN cargo new --bin ${NAME}
WORKDIR /${NAME}

RUN cargo install diesel_cli --no-default-features --features "sqlite"
COPY diesel.toml ./
ENV DATABASE_URL database.sqlite
RUN diesel setup && \
	diesel migration run

COPY Cargo.lock Cargo.toml ./
RUN cargo build --release &&\
	rm src/*.rs && \
    rm target/release/deps/$(echo $NAME | tr - _)*

COPY . .

RUN cargo build --release


FROM debian:stable-slim

RUN apt-get update && apt-get install -y libsqlite3-dev libssl1.1 openssl curl

ARG NAME=app

WORKDIR /${NAME}

COPY --from=builder /${NAME}/run.sh ./
RUN sh -c "sed 's/\$1/$NAME/' run.sh" && \
	sh -c "sed 's/\$1/$NAME/' -i run.sh"
RUN chmod +x run.sh
COPY --from=builder /${NAME}/database.sqlite /${NAME}/Rocket.toml ./
COPY --from=builder /${NAME}/target/release/${NAME} ./target/release/${NAME}

CMD ./run.sh
